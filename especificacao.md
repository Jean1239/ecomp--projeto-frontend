Projeto Frontend 2018/2
=======================

Olá. Tudo bem?

Esta é uma atividade focada em frontend, visto que é um ponto fraco da empresa atualmente. O objetivo é que você saiba se virar sem utilizar Bootstrap e bibliotecas externas para algumas coisas mais básicas, além de dar uma base para que você consiga trabalhar melhor com as "linguagens" da web.

Você deve realizar **duas** tarefas:

1. Implementar os componentes descritos na [seção abaixo](#especificação-dos-componentes) em uma página individual;

2. Criar uma **página pessoal**, com o conteúdo que você quiser, com o layout que quiser, seguindo as seguintes regras:
	- Você **não deve** utilizar bibliotecas externas, com exceção de fontes, ícones e do **JQuery**;
	- Você **deve** utilizar todos ou quase todos os componentes implementados no item **(a)**.

## Regras gerais

Antes de começar a fazer as coisas, vamos definir as regras que você deverá seguir durante todo o processo de desenvolvimento desta atividade.

Estas são as regras:
- **Nada de bibliotecas externas**: Só será permitido a utilização do [JQuery](https://jquery.com/) e de bibliotecas de fontes e ícones, como [Google Fonts](https://fonts.google.com/) e [Material Icons](https://material.io/tools/icons/);
- **Nada de .css**: Você deve desenvolver seu CSS utilizando SASS, LESS ou outro tipo de "linguagem" que compile para CSS;
- **Siga um padrão para o CSS**: Nada de nomes aleatórios para suas classes. Sempre utilize prefixos e nomes relacionados ao que aquela classe faz. Recomendo utilizar uma versão simplificada, sua, do [BEM CSS](http://getbem.com/naming/);
- **Siga um padrão para o JS**: Da mesma forma, tenha um padrão de código para JS. Qualquer empresa precisa disso, e toda empresa tem um. Dê uma olhada [nestes exemplos](https://codeburst.io/5-javascript-style-guides-including-airbnb-github-google-88cbc6b2b7aa);
- **Respeite indentação**: Saiba indentar seu código CSS, HTML e JS. Tenha um padrão, e respeite-o. Saiba que o código desenvolvido por você na empresa terá de ser lido e, por vezes, alterado por outros. Seja gentil!
- **Utilize o JS ES6**: Você deve utilizar tudo que a nova versão da linguagem tem a oferecer, para não sofrer tanto assim.
Entretanto, utilizar a versão nova **exige** que você utilize [transpiladores](https://babeljs.io/) que saibam traduzir seu JS na versão nova para versões antigas, já que o Internet Explorer e outros navegadores não a suportam ainda.
Dê uma olhada [neste guia](https://medium.com/@matheusml/o-guia-do-es6-tudo-que-voc%C3%AA-precisa-saber-8c287876325f) para entender as novas *features* da linguagem.
![Exemplo de JS ES6](js-example.png)
- **Se preocupe com acessibilidade**: Nem todos podem ver a tela do computador como você pode. Eles precisam de leitores de tela que tentam ler sua página, e os usuários devem navegá-la usando TABs e setas do teclado **apenas**. Não é difícil se você se preocupar desde o início. Dê uma lida [neste artigo da Mozilla](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/HTML) para mais informações.
- **Utilize ícones**: Sempre opte por ícones com texto, ou apenas ícones, principalmente quando estiver fazendo o layout mobile. Por que? Olhe para o seu celular, e veja quantos botões tem apenas texto. Utilize o *Font Awesome* ou o *Material Icons*. Eu particularmente recomendo o *Material Icons*, conforme será discutido mais abaixo. :)
- **Utilize o ambiente que preparamos para você**: Facilitaremos toda a parte de compilar CSS/JS e testar sua atividade, não se preocupe. Mas precisamos que você a utilize. A próxima seção abordará com mais detalhes esse assunto.

**TUDO CONTA PONTOS**. Para esta atividade, serão avaliados vários pontos e você receberá um feedback com as notas individuais, para saber onde melhorar. Se esforce!

Os componentes a serem desenvolvidos estão disponíveis [nesta página](http://www.inf.ufpr.br/fas16/secret/projeto-frontend/home/), mas leia a [seção de especificação](#especificação-dos-componentes) primeiro.

**Sobre o Material Icons**: trata-se de uma [biblioteca gratuita](https://material.io/tools/icons/), desenvolvida pela Google, que tem vários dos ícones utilizados no seu celular. O contraponto é que redes sociais não permitem que seus ícones sejam adicionados à essa biblioteca. Se decidir por utilizar o *Material Icons*, leia a seção ["Sobre o Material Icons"](#sobre-o-material-icons).

## Acalme-se

Falamos de JS transpilado, CSS compilado e tudo o mais, mas não queremos que você aprenda a preparar esses compiladores e transpiladores agora. Por isso, preparamos um ambiente que faz isso por você, sem auê.

O ambiente está disponível [neste projeto](https://gitlab.ecomp.co/fshinohata/boilerplate-projeto-frontend-2018-2). Faça um *fork* para sua conta para começar a desenvolver.

### Pré-requisitos para utilizar o ambiente

Você precisa ter uma versão relativamente recente do [NodeJS](https://nodejs.org/en/) instalado em sua máquina. Após instalá-lo, você deve instalar também o [GulpJS](https://gulpjs.com/) globalmente, com o seguinte comando:

```bash
# para linux
sudo npm install -g gulp-cli

# para windows
npm install -g gulp-cli
```

Após realizar o *fork* do ambiente e clonar seu projeto, abra a pasta clonada e execute `npm install` para instalar as dependências. Com isso, tudo deve funcionar.

Os comandos disponíveis são os seguintes:
- `npm start`: Compila todos os seus arquivos e inicia o servidor de testes. Seu terminal ficará congelado com o servidor rodando;
- `npm run build`: Compila todos os arquivos uma única vez, e encerra a execução.

### Como funciona o ambiente

Todos os arquivos-fonte devem ficar na pasta `src/`. Todos os arquivos desta pasta são compilados/copiados e jogados para a pasta `build/`, que é criada automagicamente quando você executar algum dos comandos acima.

Dentro da pasta `src/`, você terá **quatro pastas**:
- `img/`: Onde você deve colocar suas imagens. Elas são copiadas para a pasta `build/img/`. Se você criar uma pasta, ela também será criada no diretório `build/img/`;
- `js/`: Onde você deve desenvolver seus arquivos JS. Todos os arquivos serão compilados e jogados para a pasta `build/js/`. Você pode criar pastas para se organizar, mas tenha em mente que elas serão eliminadas na compilação e os arquivos dentro delas serão compilados e jogados para `build/js/`. Ou seja, se você tem o arquivo `src/js/a.js` e o arquivo `src/js/pasta/b.js`, o resultado final serão os arquivos `build/js/a.js` e `build/js/b.js`;
- `scss/`: Onde você deve desenvolver seus arquivos SASS. Todos os arquivos serão compilados e jogados para a pasta `build/css/`. Aqui, da mesma forma, subpastas são eliminadas e todos os arquivos ficarão diretamente sobre a pasta `build/css/`;
- `pages/`: Onde você deve desenvolver seus arquivos HTML. Se você não quiser sofrer muito, saiba que o compilador está configurado para aceitar arquivos `.pug`, do [PugJS](https://pugjs.org/api/getting-started.html). Os arquivos desta pasta são tratados um pouco diferente: o arquivo `src/pages/index.{html,pug}` será jogado em `build/index.html`; o arquivo `src/pages/home/home.{html,pug}` será jogado em `build/home/home.html`; ou seja, toda a estrutura de arquivos diretamente abaixo de `src/pages/` será copiada para `build/`.

Tenha em mente que seus arquivos `.{html,pug}` devem referenciar os arquivos da pasta `build/`. Pense no futuro: Se o seu arquivo `src/pages/home/index.html` (que se tornará `build/home/index.html`) precisa do arquivo `src/scss/home.scss` (que se tornará `build/css/home.css`), então o link que você precisa colocar na página será:

```html
<!-- A rota "/" referencia a pasta "build/" -->
<link rel="stylesheet" href="/css/home.css">
```

Se sentir dificuldade, peça ajuda para algum dos membros de projetos. :)

## Especificação dos componentes

O mínimo de componentes que você deve desenvolver são:
- Uma **navbar colapsível**, ou seja, cujos botões "somem" e são transformados em uma **sidebar** (veja exemplo [na página](http://www.inf.ufpr.br/fas16/secret/projeto-frontend/home/)). As especificações são essas:
	- A navbar deve conter ao menos três botões (não precisam ser links definidos), visíveis apenas para telas maiores que 425px;
	- A navbar deve mostrar um [hamburger](https://www.google.com.br/search?q=hamburger+menu&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiWj6jJ7YneAhVDiZAKHSA2CuYQ_AUIDigB&biw=1358&bih=623) para telas menores ou iguais a 425px;
	- Ao clicar no hamburger, uma **sidebar** com os botões originais da navbar deve aparecer e ocupar ao menos 50% da tela;
	- O aparecimento/desaparecimento da sidebar **deve ser animado**.
- Um **carrossel**, com um layout de sua escolha, com as seguintes especificações:
	- Funções de **Alterar slide automaticamente** e **alterar slide via controle**, que devem ser "escolhíveis" (facilmente definidas para cada carrossel);
	- **Mudança de slide animada**;
	- Esse componente **não deve ser estático**, ou seja, deve funcionar independente do número de imagens, se adaptar ao tamanho da tela e deve permitir a inclusão de vários numa mesma página.
- Botões com cores padrão **primary**, **secondary**, **danger**, **warning** e **info**. As cores devem ser semelhantes às padrões do *Bootstrap*;
![Cores padrão do Bootstrap](bootstrap-colors.png)
- Todo e qualquer botão/link deve mudar de cor (do jeito que você definir) durante um evento de *hover* (*e.g.* quando o mouse passar por cima);
- Alertas com cores padrão **primary**, **danger**, **warning** e **info**;
- Classes CSS que permitam utilizar facilmente o *flexbox*, conforme exemplos [da página](http://www.inf.ufpr.br/fas16/secret/projeto-frontend/home/). Aliás, **você deve desenvolver os exemplos da página com seu CSS**;
- Um componente que implemente um *Photo Grid* (veja exemplo [na página](http://www.inf.ufpr.br/fas16/secret/projeto-frontend/home/)). Esse componente **não deve ser estático**, ou seja, deve funcionar independente do número de imagens, se adaptar ao tamanho da tela e deve permitir a inclusão de vários numa mesma página;
- Implementação de [**Parallax**](https://www.w3schools.com/howto/tryhow_css_parallax_demo.htm), que deve funcionar para Desktop, pelo menos;
- Um **footer**, que deve ter comportamento semelhante ao da página exemplo e seguir as seguintes espeficicações:
	- Deve conter uma lista de links relacionados ao tema "Sobre nós";
	- Deve conter uma lista de links relacionados ao tema "Redes Sociais";
	- Deve conter informações de endereço e contato;
	- Ao final, deve conter um *disclaimer*/*copyright* **com o nome do desenvolvedor**.

Todos os componentes devem ter um layout para:
- Celular (telas de 0 até 425px de largura);
- Tablet (telas de 426px até 767px de largura);
- Laptop e Desktop (telas de 768px e acima de largura);
- **(Opcional)** 4K (telas de 2560px e acima de largura).

Qualquer navegador ~~decente~~ moderno possui um modo de design mobile, onde você pode testar sua aplicação em diferentes tamanhos de tela. É preferível que você acesse sua aplicação pelo seu celular real também, porque o navegador do celular é diferente do desktop, então algumas coisas podem se comportar de maneira diferente.

No Google Chrome e no Firefox, você pode ativar o modo de design mobile abrindo o *Developer Tools* com `ctrl+shift+I` e então ativando o modo com `ctrl+shift+m`. Se você utiliza o Google Chrome, dê uma olhada no pacote [Emmet Re:View](https://chrome.google.com/webstore/detail/emmet-review/epejoicbhllgiimigokgjdoijnpaphdp?hl=en) e facilite sua vida.

## Um pequeno presente

O projeto da página com os exemplos de componente está disponível [neste link](https://gitlab.com/fshinohata/ecomp--projeto-frontend). O código fonte está na pasta `src/`, e tudo funciona da mesma forma que o ambiente dado a vocês.

Sintam-se à vontade para dar *fork* e explorar o projeto, mas lembrem-se que o objetivo final de todas as atividades é que vocês tenham **aprendido a fazer** o que foi pedido, e não **aprendido a copiar** o que foi pedido. Nossa recomendação é que, durante o período de desenvolvimento, você só tente ler e entender o código se você estiver realmente travado e nenhuma pesquisada no Google esteja ajudando.

## Sugestões para estudo/desenvolvimento

Aqui está uma lista de links que podem lhe ajudar nessa jornada:
- [Cursinho de JS ES5 do W3Schools](https://www.w3schools.com/js/default.asp)
- [Features do JavaScript ES6](https://medium.com/@matheusml/o-guia-do-es6-tudo-que-voc%C3%AA-precisa-saber-8c287876325f)
- [Geralzão de CSS do W3Schools](https://www.w3schools.com/css/default.asp)
- [Página-exemplo com os componentes especificados](http://www.inf.ufpr.br/fas16/secret/projeto-frontend/home/)
- [Guia de Acessibilidade da Mozilla Developers Network](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/HTML)
- [Emmet Re:View para Google Chrome](https://chrome.google.com/webstore/detail/emmet-review/epejoicbhllgiimigokgjdoijnpaphdp?hl=en)
- [Keeping it Simple -- Coding a Carousel](https://christianheilmann.com/2015/04/08/keeping-it-simple-coding-a-carousel/)
- [How to Create a Slideshow/Carousel](https://www.w3schools.com/howto/howto_js_slideshow.asp)
- [Parallax que funciona em Desktop](https://www.w3schools.com/howto/howto_css_parallax.asp)
- [Padrão BEM CSS](http://getbem.com/naming/)
- [Padrões de Código JS](https://codeburst.io/5-javascript-style-guides-including-airbnb-github-google-88cbc6b2b7aa)
- [Material Icons](https://material.io/tools/icons/)
- [PugJS](https://pugjs.org/api/getting-started.html)

## Sobre o Material Icons

Como já dito, o *Material Icons* é uma biblioteca *Open Source* de ícones que é amplamente utilizada. Essa biblioteca vem em duas formas: Um `.zip` com todos os ícones em formatos de imagem (que pesa cerca de 60MB), e uma fonte que pesa no máximo 60KB.

A fonte é muito mais simples de utilizar, e obviamente será a abordada nesta seção.

Inclua esse link no cabeçalho de suas páginas para utilizar a fonte:

```html
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
```

Para utilizá-la, basta criar um elemento `<i>` com a classe "material-icons", e dentro desse elemento colocar o nome do ícone:

```html
<i class="material-icons">people</i>
<i class="material-icons">accessibility</i>
<i class="material-icons">android</i>
```

Para alterar a cor de um ícone, basta adicionar uma classe ao elemento que altere o atributo `color`.

Se quiser mais flexibilidade para customizar o tamanho e cor dos ícones, adicione as seguintes regras em um de seus arquivos CSS:

```css
/* Rules for sizing the icon. */
.material-icons.md-18 { font-size: 18px; }
.material-icons.md-24 { font-size: 24px; }
.material-icons.md-36 { font-size: 36px; }
.material-icons.md-48 { font-size: 48px; }

/* Rules for using icons as black on a light background. */
.material-icons.md-dark { color: rgba(0, 0, 0, 0.54); }
.material-icons.md-dark.md-inactive { color: rgba(0, 0, 0, 0.26); }

/* Rules for using icons as white on a dark background. */
.material-icons.md-light { color: rgba(255, 255, 255, 1); }
.material-icons.md-light.md-inactive { color: rgba(255, 255, 255, 0.3); }
```

E utilize:

```html
<i class="material-icons md-24 md-dark">people</i>
<i class="material-icons md-36">accessibility</i>
<i class="material-icons md-48 md-dark md-inactive">android</i>
```

Consulte o nome dos ícones [na página oficial](https://material.io/tools/icons/).

Mais informações sobre a biblioteca está disponível [neste link](https://google.github.io/material-design-icons/).

## Relembrando: O que deve ser feito, e o que será cobrado?

A fazer:
- Uma página com as implementações dos [componentes](#especificação-dos-componentes);
- Uma página pessoal, sua, com o que quiser, layout que quiser, usando todos ou quase todos os componentes implementados.

O que será cobrado:
- Qualidade das animações;
- Qualidade do funcionamento dos componentes;
- Facilidade de leitura do código;
- Acessibilidade do código;
- Seguimento de algum padrão;
- Implementação do mínimo que foi especificado (você sempre pode fazer mais!).

## Entrega

Ao terminar a atividade, envie apenas os seguintes arquivos:
- A pasta `src/`, **com todos os seus arquivos-fonte**;
- Seu arquivo `package.json`, **se você instalar algum pacote extra**;
- Seu arquivo `gulpfile.js`, **se você alterar o original**.

Todos eles devem ser enviados em um arquivo compactado (`.zip`, `.tar.gz`, etc) para o email `projetos.ecomp@gmail.com`.

O **assunto do email** deve ser "Entrega do Projeto Frontend 2018/2".

No **corpo do email**, escreva seu nome completo e **DEIXE BEM CLARO** se você permite ou não que divulguemos sua nota publicamente para a empresa. Se você optar por não publicar, você ainda receberá um email com feedback individual.
