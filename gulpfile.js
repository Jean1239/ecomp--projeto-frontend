var gulp = require('gulp');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var flatten = require('gulp-flatten');
var clean = require('gulp-clean');
var minifyCSS = require('gulp-minify-css');
var minifyJS = require('gulp-minify');
var babel = require('gulp-babel');
var browserSync = require('browser-sync');

var server = browserSync.create();

var env = require('./env.json');
var baseUrl = env.baseUrl;

function logError(err) {
    console.log(err.message);
    this.emit('end');
}

gulp.task('serve', function () {
	var files = [
		'build/**/*'
	];
	server.init(files, {
		server: {
			baseDir: './build'
		}
	});
});

gulp.task('scss', function () {
	gulp.src('src/scss/**/*.scss')
        .pipe(sass())
        .on('error', logError)
        .pipe(minifyCSS())
        .on('error', logError)
        .pipe(flatten())
        .on('error', logError)
		.pipe(gulp.dest('build/css'));
});

gulp.task('html', function () {
	gulp.src('src/pages/**/*.html', { base: 'src/pages' })
		.pipe(gulp.dest('build'));
	gulp.src(['src/pages/**/*.pug', '!src/pages/**/_*.pug'], { base: 'src/pages' })
        .pipe(pug({ locals: { baseUrl } }))
        .on('error', logError)
		.pipe(gulp.dest('build'));
});

gulp.task('js', function () {
	gulp.src('src/js/**/*.js')
		.pipe(babel({
			presets: ['@babel/env']
        }))
        .on('error', logError)
        .pipe(flatten())
        .on('error', logError)
		.pipe(gulp.dest('build/js'));
});

gulp.task('img', function () {
    gulp.src('src/img/**/*.{jpg,jpeg,png,gif}', { base: 'src/img' })
        .pipe(gulp.dest('build/img'));
});

gulp.task('clean', function () {
	return gulp.src('build/*', { read: false })
		.pipe(clean());
});

gulp.task('build', ['clean'], function (done) {
	gulp.start(['js', 'scss', 'img', 'html']);
	done();
});

gulp.task('default', ['build'], function () {
	gulp.start('serve');
	gulp.watch('src/js/**/*.js', ['js']);
	gulp.watch(['src/pages/**/*.html', 'src/pages/**/*.pug'], ['html']);
	gulp.watch('src/scss/**/*.scss', ['scss']);
	gulp.watch('src/scss/**/*.{jpg,jpeg,png,gif}', ['img']);
});
