class Layout {
	static init() {
		if (Layout.initialized === true) {
			throw new Layout.MultipleInitializationException();
		}
		Layout.initialized = true;

		let navbarItems = Array.from(document.querySelectorAll('.navbar-item'));
		let navbar = document.querySelector('.navbar');
		let sidebar = document.createElement('div');
		sidebar.classList.add('sidebar');
		navbarItems.forEach(item => {
			let copy = item.cloneNode(true);
			copy.classList.replace('navbar-item', 'sidebar-item');
			sidebar.appendChild(copy);
		});
		navbar.appendChild(sidebar);

		let navbarBurger = document.querySelector('.navbar-burger');
		navbarBurger.addEventListener('click', () => {
			sidebar.classList.toggle('active');
		});

		let backToTopElements = Array.from(document.querySelectorAll('.navbar [data-action="back-to-top"]'));
		backToTopElements.forEach(element => {
			element.addEventListener('click', () => {
				window.scrollTo(0,0);
			});
		});
	}

	static MultipleInitializationException(message) {
		return {
			error: 'layout_multiple_initialization',
			message: message || "Attempt to initialize Layout class more than once."
		};
	}
}

Layout.init();