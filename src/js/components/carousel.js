class CarouselManager {
    static init(options = {}) {
        if (CarouselManager.initialized === true) {
            throw new MultipleInitializationException();
        }
        CarouselManager.initialized = true;
        CarouselManager.carousels = [];
        var containers = document.querySelectorAll('.carousel');
        containers.forEach(container => CarouselManager.carousels.push(new Carousel(container, CarouselManager.getOptions(container))));
    }

    static getOptions(container) {
        let autoPlayAttr = container.getAttribute('auto-play');
        let autoPlayTime = autoPlayAttr != null ? parseFloat(autoPlayAttr) : 0;
        return {
            renderControls: container.getAttribute('with-controls') != null,
            animatedHover: container.getAttribute('animated-hover') != null,
            autoPlay: {
                enable: autoPlayAttr != null,
                time: autoPlayTime >= 1 ? autoPlayTime : 1,
            }
        };
    }
}

class Carousel {
    constructor(container, options = { renderControls: false, animatedHover: false, autoPlay: { enable: false, time: 0 } }) {
        if (!(container instanceof Element)) {
            throw new InvalidCarouselContainerException();
        }
        this.ANIMATION_TIME = 1100;
        this.animations = [
            'animated-hover',
        ];
        this.container = container;
        this.images = [];
        let images = container.querySelectorAll('img');
        images.forEach((image, index) => this.images.push({index, element: image}));

        this.imageInSpotlight = this.images[0];
        this.renderControls = options.renderControls;
        this.animatedHover = options.animatedHover;
        this.autoPlay = options.autoPlay;

        this.renderContainer = this.renderContainer.bind(this);
        this.renderImageContainer = this.renderImageContainer.bind(this);
        this.getImagePosition = this.getImagePosition.bind(this);
        this.renderControlsIfNecessary = this.renderControlsIfNecessary.bind(this);
        this.removeAnimations = this.removeAnimations.bind(this);
        this.addAnimations = this.addAnimations.bind(this);
        this.changeImageInSpotlight = this.changeImageInSpotlight.bind(this);
        this.removeImageFromSpotlight = this.removeImageFromSpotlight.bind(this);
        this.putImageInSpotlight = this.putImageInSpotlight.bind(this);

        let { controls: { leftArrow, rightArrow }, slides } = this.renderContainer();

        this.leftArrow = leftArrow;
        this.rightArrow = rightArrow;
        this.slides = slides;
        this.allowSwitchSlide = true;

        if (this.renderControls) {
            this.leftArrow.addEventListener('click', () => {
                this.changeImageInSpotlight(-1);
            });

            this.rightArrow.addEventListener('click', () => {
                this.changeImageInSpotlight(+1);
            });
        }

        if (this.autoPlay.enable) {
            this.autoPlayInterval = setInterval(() => {
                this.changeImageInSpotlight(+1);
            }, this.autoPlay.time * 1000);
        }
    }

    changeImageInSpotlight(offset = 0) {
        if (!this.allowSwitchSlide) return;
        this.allowSwitchSlide = false;
        this.removeImageFromSpotlight();
        let nextIndex = this.getImageIndex(this.imageInSpotlight.index, offset);
        this.putImageInSpotlight(nextIndex);
        setTimeout(() => { this.allowSwitchSlide = true; }, this.ANIMATION_TIME);
    }

    removeImageFromSpotlight() {
        let leftIndex = this.getImageIndex(this.imageInSpotlight.index, -1);
        let rightIndex = this.getImageIndex(this.imageInSpotlight.index, +1);
        let leftSlide = this.slides[leftIndex];
        let centerSlide = this.slides[this.imageInSpotlight.index];
        let rightSlide = this.slides[rightIndex];
        leftSlide.classList.remove('left');
        centerSlide.classList.remove('center');
        rightSlide.classList.remove('right');
        this.removeAnimations(centerSlide);
    }

    putImageInSpotlight(index) {
        this.imageInSpotlight = this.images[index];
        let leftIndex = this.getImageIndex(this.imageInSpotlight.index, -1);
        let rightIndex = this.getImageIndex(this.imageInSpotlight.index, +1);
        let leftSlide = this.slides[leftIndex];
        let centerSlide = this.slides[this.imageInSpotlight.index];
        let rightSlide = this.slides[rightIndex];
        leftSlide.classList.add('left');
        centerSlide.classList.add('center');
        rightSlide.classList.add('right');
        this.addAnimations(centerSlide);
    }

    getImageIndex(index, offset) {
        return (index + offset) >= 0 ? (index + offset) % this.images.length : this.images.length + (index + offset);
    }

    getImagePosition(image) {
        if (image.index === this.getImageIndex(this.imageInSpotlight.index, -1)) {
            return " left";
        } else if (image.index === this.imageInSpotlight.index) {
            return " center";
        } else if (image.index === this.getImageIndex(this.imageInSpotlight.index, +1)) {
            return " right";
        }
        return "";
    }

    removeAnimations(slide) {
        this.animations.forEach(animation => slide.classList.remove(animation));
    }

    addAnimations(slide) {
        if (this.animatedHover) {
            slide.classList.add('animated-hover');
        }
    }

    renderControlsIfNecessary() {
        if (!this.renderControls) {
            return "";
        }
        return `
            <div class="carousel-controls" aria-label="Controles do carrossel">
                <div role="button" class="carousel-controls__left-arrow" aria-label="Ver slide à esquerda">
                    <i class="material-icons md-48">keyboard_arrow_left</i>
                </div>
                <div role="button" class="carousel-controls__right-arrow" aria-label="Ver slide à direita">
                    <i class="material-icons md-48">keyboard_arrow_right</i>
                </div>
            </div>
        `;
    }

    renderImageContainer(image) {
        return `
            <div role="slide" tabindex="0" class="carousel-slide${this.getImagePosition(image)}" aria-label="Slide do carrossel">
            ${image.element.outerHTML}
            </div>
        `;
    }

    renderImages() {
        let images = "";
        this.images.forEach(image => images += this.renderImageContainer(image));
        return images;
    }

    renderContainer() {
        this.container.classList.remove('carousel');
        this.container.classList.add('carousel-container');
        this.container.innerHTML = `
            <div role="presentation" tabindex="0" class="carousel-frame" aria-label="Contêiner do carrossel">
            ${this.renderControlsIfNecessary()}
            ${this.renderImages()}
            </div>
        `;

        let leftArrow = this.container.querySelector('.carousel-controls__left-arrow');
        let rightArrow = this.container.querySelector('.carousel-controls__right-arrow');
        let slides = Array.from(this.container.querySelectorAll('.carousel-slide'));

        this.addAnimations(slides[this.imageInSpotlight.index]);

        return {
            controls: {
                leftArrow, rightArrow
            },
            slides
        };
    }
}

class InvalidCarouselContainerException {
    constructor(message) {
        this.error = "invalid_carousel_container";
        this.message = message || "Attempt to instantiate a Carousel with a invalid container. The container must be an instance of HTMLElement.";
    }
}

class MultipleInitializationException {
    constructor(message) {
        this.error = "multiple_initialization";
        this.message = message || "Attempt to initialize CarouselManager more than once.";
    }
}
