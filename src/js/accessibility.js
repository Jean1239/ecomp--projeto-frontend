class AccessibilityManager {
	static init() {
		const selectors = {
			".frame": {
				"aria-label": "Contêiner de conteúdo do site",
			},
			".alert": {
				"role": "alert",
				"tabindex": "0",
			},
			".aria-hidden": {
				"aria-hidden": "true",
			},
			".image-gallery": {
				"aria-label": "Galeria de imagens",
				"tabindex": "0",
			},
			"img": {
				"tabindex": "0",
			},
			".parallax": {
				"tabindex": "0",
			},
			".navbar-heading a": {
				"role": "button",
				"aria-label": "Link para a página inicial com o nome do site",
				"tabindex": "0",
			},
			".navbar-heading h1": {
				"role": "heading",
				"aria-label": "Nome do site",
			},
			".navbar-item": {
				"role": "button",
			},
			".navbar-item[data-action=back-to-top]": {
				"aria-hidden": "true",
			},
		};

		for (var selector in selectors) {
			let elements = Array.from(document.querySelectorAll(selector));
			elements.forEach(element => {
				for (var attribute in selectors[selector]) {
					element.setAttribute(attribute, selectors[selector][attribute]);
				}
			});
		}
	}
}

AccessibilityManager.init();
